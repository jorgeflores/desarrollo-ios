//
//  main.m
//  EjemploModal
//
//  Created by Jorge Flores on 3/7/14.
//  Copyright (c) 2014 Jorge Flores. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOAppDelegate class]));
    }
}
